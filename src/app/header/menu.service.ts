import { Subject } from 'rxjs';

export class MenuService {
  isOpen = false;
  statusChange = new Subject<boolean>();

  toggle() {
    this.isOpen = !this.isOpen;
    this.statusChange.next(this.isOpen);
  }
}
