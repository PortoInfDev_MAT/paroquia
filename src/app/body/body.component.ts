import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { Person } from '../models/person';
import { Subscription } from 'rxjs';
import { MenuService } from '../header/menu.service';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  pListObj = [];
  menuStatus = false;

  menuSub: Subscription;

  constructor(
    @Inject(DOCUMENT) private document: any,
    private menuService: MenuService
  ) {}

  ngOnInit() {
    this.menuSub = this.menuService.statusChange.subscribe(isOpen => {
      this.menuStatus = isOpen;
    });
  }

  toogleMenu() {
    this.menuStatus = !this.menuStatus;
  }

  fileUpload(event: any) {
    localStorage.clear();

    const reader = new FileReader();
    const personsList = [];

    reader.readAsText(event.srcElement.files[0]);

    reader.onload = function() {
      const lines = reader.result.toString().split('\n');

      for (let index = 0; index < lines.length; index++) {
        const element = lines[index];
        const splitElement = element.split(',');
        personsList.push(splitElement);
      }
    };

    reader.onloadend = () => {
      personsList.forEach(element => {
        if (element.length === 5) {
          const person = new Person();
          person.name = element[0];
          person.address = element[1];
          person.lat = element[2];
          person.long = element[3];
          person.cat = element[4].toLowerCase();

          this.pListObj.push(person);
        }
      });

      localStorage.setItem('pdb', JSON.stringify(this.pListObj));
      localStorage.setItem('persons', JSON.stringify(this.pListObj));
    };
  }

  getAll() {
    localStorage.setItem('persons', localStorage.getItem('pdb'));
    this.document.location.href = this.document.location.href;
  }

  getFilter(event: any) {
    console.log(event.textContent.toLowerCase());

    const fullList = JSON.parse(localStorage.getItem('pdb'));
    const tmpList = [];

    fullList.forEach((_element: Person) => {
      if (
        _element.cat.toLowerCase().includes(event.textContent.toLowerCase()) ||
        _element.cat.toLowerCase().includes('main')
      ) {
        tmpList.push(_element);
      }
    });

     localStorage.setItem('persons', JSON.stringify(tmpList));
     this.document.location.href = this.document.location.href;
  }

}
