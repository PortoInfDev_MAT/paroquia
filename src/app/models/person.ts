export class Person {
  name: string;
  address: string;
  location: string;
  lat: number;
  long: number;
  cat: string;
}
