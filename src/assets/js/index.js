tomtom.setProductInfo('Paroquia', '1.0.0');

var data = JSON.parse(localStorage.getItem('persons'));

var addressPoints = [];

const setPoints = () => {
  if (data) {
    data.forEach(element => {
      addressPoints.push([element.lat, element.long, element.name, element.cat]);
    });
  }
};

setPoints();

console.log(addressPoints);

const run = () => {
  // Creating the map
  var map = tomtom.L.map('map', {
    key: 'ggx4fhsnYzLuWu3pjWRO4ZTZKEKFj2RZ',
    source: 'vector',
    basePath: 'https://api.tomtom.com/maps-sdk-js/4.46.3/examples/sdk'
  }).setView([41.182821, -8.654698], 15);

  // Placing markers
  var markers = tomtom.L.markerClusterGroup();
  addressPoints.forEach(function (point) {
    var iconPath = '';
    switch (point[3].trim()) {
      case 'main':
        iconPath = 'assets/img/main.png';
        break;
      case 'informatica':
        iconPath = 'assets/img/it.png';
        break;
      case 'catecumeno':
        iconPath = 'assets/img/cat.png';
        break;
      case 'cne':
        iconPath = 'assets/img/default.png';
        break;
      default:
        iconPath = 'assets/img/default.png';
        break;
    }

    var markerOptions = {
      icon: tomtom.L.icon({
        iconUrl: iconPath,
        iconSize: [30, 34],
        iconAnchor: [15, 34]
      }),
      title: title
    };

    var title = point[2],
      marker = tomtom.L.marker(new tomtom.L.LatLng(point[0], point[1]), markerOptions);
    marker.bindPopup(title);
    markers.addLayer(marker);
  });

  map.addLayer(markers);

};

run();
